#!/bin/bash

###################
apt-get -y update
apt-get -y install \
  wget \
  nginx \
  unzip
###################
cd /srv
###################
wget https://kiwiirc.com/downloads/kiwiirc_${kiwiirc_version}_linux_amd64.zip
unzip kiwiirc_${kiwiirc_version}_linux_amd64.zip
rm *.zip
mv * kiwiirc
###################
useradd kiwi
cp /var/tmp/config.conf /srv/kiwiirc/config.conf
cp /var/tmp/client.json /srv/kiwiirc/www/static/config.json

